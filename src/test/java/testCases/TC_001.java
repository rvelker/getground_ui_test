package testCases;

//    TC 1
//    age     < 30
//    partner     NO
//    benefits    YES
//    universal credit    YES
//    universal credit special circumstances      YES
//    take-home pay < 935     YES

import base.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.costsChecker.*;
import pageObjects.costsChecker.benefitsPages.BenefitsPage;
import pageObjects.costsChecker.benefitsPages.TakeHomePayPage;
import pageObjects.costsChecker.benefitsPages.UCSpecialCircumstancesPage;
import pageObjects.costsChecker.benefitsPages.UniversalCreditPage;
import pageObjects.costsChecker.resultsPages.ResultsUniversalCreditPage;

public class TC_001 extends TestBase {

    @Test
    void validate_use_case_001_can_input_circumstances_and_view_results(){
        Assert.assertEquals(driver.getCurrentUrl(), startPage.url, "Current url is different from expected");

        startPage.beginCheck();

        interactions.selectCountryWales();

        interactions.selectDateOfBirthUnderThirty();

        interactions.selectPartnerNo();

        interactions.selectReceiveBenefitsYes();

        interactions.selectReceiveUniversalCreditYes();

        interactions.selectUniversalCreditCircumstanceYes();

        interactions.selectTakeHomePayLessThanAmountYes();

        interactions.assertUniversalCreditResultsAreDisplayed();
    }
}
