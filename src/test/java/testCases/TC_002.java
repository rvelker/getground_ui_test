package testCases;

import base.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.costsChecker.*;
import pageObjects.costsChecker.benefitsPages.BenefitsPage;
import pageObjects.costsChecker.benefitsPages.TakeHomePayPage;
import pageObjects.costsChecker.benefitsPages.UCSpecialCircumstancesPage;
import pageObjects.costsChecker.benefitsPages.UniversalCreditPage;
import pageObjects.costsChecker.resultsPages.ResultsPage;

//TC 2
//        age     > 30
//        partner     YES
//        benefits    YES
//        universal credit    YES
//        universal credit special circumstances      YES
//        take-home pay < 935     NO
//        pregnancy     NO
//        injury      YES
//        diabetes  YES
//        glaucoma  YES
//        care home   YES
//        council help    NO
//        savings > 24k       YES

public class TC_002 extends TestBase {

    @Test
    void validate_use_case_002_can_input_circumstances_and_view_results(){
        Assert.assertEquals(driver.getCurrentUrl(), startPage.url,
                "Driver should be at start page url");

        startPage.beginCheck();

        interactions.selectCountryWales();

        interactions.selectDateOfBirthOverThirty();

        interactions.selectPartnerYes();

        interactions.selectReceiveBenefitsYes();

        interactions.selectReceiveUniversalCreditYes();

        interactions.selectUniversalCreditCircumstanceYes();

        interactions.selectTakeHomePayLessThanAmountNo();

        interactions.selectPregnancyNo();

        interactions.selectInjuryYes();

        interactions.selectDiabetesYes();

        interactions.selectGlaucomaYes();

        interactions.selectCareHomeYes();

        interactions.selectCouncilHelpNo();

        interactions.selectSavingsOverAmountYes();

        interactions.assertResultsAreDisplayed();
    }
}
