package base;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageObjects.costsChecker.*;
import pageObjects.costsChecker.benefitsPages.BenefitsPage;
import pageObjects.costsChecker.benefitsPages.TakeHomePayPage;
import pageObjects.costsChecker.benefitsPages.UCSpecialCircumstancesPage;
import pageObjects.costsChecker.benefitsPages.UniversalCreditPage;
import pageObjects.costsChecker.resultsPages.ResultsPage;
import pageObjects.costsChecker.resultsPages.ResultsUniversalCreditPage;

public class UiInteractions {

    private WebDriver driver;

    public UiInteractions(WebDriver driver) {
        this.driver = driver;
    }

    public void selectCountryWales() {
        CountryPage page = new CountryPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectWales();
        System.out.println("Selected Wales");
        page.clickNext();
    }

    public void selectDateOfBirthUnderThirty() {
        DateOfBirthPage page = new DateOfBirthPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.enterDateOfBirthUnderThirty();
        System.out.println("Selected under thirty");
        page.clickNext();
    }

    public void selectDateOfBirthOverThirty() {
        DateOfBirthPage page = new DateOfBirthPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.enterDateOfBirthUnderThirty();
        System.out.println("Selected over thirty");
        page.clickNext();
    }

    public void selectPartnerYes() {
        PartnerPage page = new PartnerPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectYes();
        System.out.println("Selected lives with partner");
        page.clickNext();
    }

    public void selectPartnerNo() {
        PartnerPage page = new PartnerPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectYes();
        System.out.println("Selected lives without partner ");
        page.clickNext();
    }

    public void selectReceiveBenefitsYes() {
        BenefitsPage page = new BenefitsPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectYes();
        System.out.println("Selected yes receiving benefits");
        page.clickNext();
    }

    public void selectReceiveBenefitsNo() {
        BenefitsPage page = new BenefitsPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectNo();
        System.out.println("Selected not receiving benefits");
        page.clickNext();
    }

    public void selectReceiveUniversalCreditYes() {
        UniversalCreditPage page = new UniversalCreditPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectYes();
        System.out.println("Selected yes receiving universal credit");
        page.clickNext();
    }

    public void selectReceiveUniversalCreditNotYet() {
        UniversalCreditPage page = new UniversalCreditPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectNotYet();
        System.out.println("Selected applied, but not yet receiving universal credit");
        page.clickNext();
    }

    public void selectReceiveUniversalCreditDifferentBenefit() {
        UniversalCreditPage page = new UniversalCreditPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectDifferentBenefits();
        System.out.println("Selected receiving different benefit, not universal credit");
        page.clickNext();
    }

    public void selectUniversalCreditCircumstanceYes() {
        UCSpecialCircumstancesPage page = new UCSpecialCircumstancesPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectYes();
        System.out.println("Selected yes to universal credit special circumstances");
        page.clickNext();
    }

    public void selectUniversalCreditCircumstanceNo() {
        UCSpecialCircumstancesPage page = new UCSpecialCircumstancesPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectNo();
        System.out.println("Selected no to universal credit special circumstances");
        page.clickNext();
    }

    public void selectTakeHomePayLessThanAmountYes() {
        TakeHomePayPage page = new TakeHomePayPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectYes();
        System.out.println("Selected yes to take-home pay is less than X");
        page.clickNext();
    }

    public void selectTakeHomePayLessThanAmountNo() {
        TakeHomePayPage page = new TakeHomePayPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectNo();
        System.out.println("Selected no to take-home pay is less than X");
        page.clickNext();
    }

    public void selectInjuryYes() {
        InjuryPage page = new InjuryPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectYes();
        System.out.println("Selected yes to injury");
        page.clickNext();
    }

    public void selectInjuryNo() {
        InjuryPage page = new InjuryPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectNo();
        System.out.println("Selected no to injury");
        page.clickNext();
    }

    public void selectCareHomeYes() {
        CareHomePage page = new CareHomePage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectYes();
        System.out.println("Selected yes to lives in care home");
        page.clickNext();
    }

    public void selectCareHomeNo() {
        CareHomePage page = new CareHomePage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectNo();
        System.out.println("Selected no to lives in care home");
        page.clickNext();
    }

    public void selectCouncilHelpYes() {
        CouncilHelpPage page = new CouncilHelpPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectYes();
        System.out.println("Selected yes to council help with care home costs");
        page.clickNext();
    }

    public void selectCouncilHelpNo() {
        CouncilHelpPage page = new CouncilHelpPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectNo();
        System.out.println("Selected no to council help with care home costs");
        page.clickNext();
    }

    public void selectSavingsOverAmountYes() {
        SavingsPage page = new SavingsPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectYes();
        System.out.println("Selected yes to savings over X");
        page.clickNext();
    }

    public void selectSavingsOverAmountNo() {
        SavingsPage page = new SavingsPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectNo();
        System.out.println("Selected yes to savings over X");
        page.clickNext();
    }

    public void selectPregnancyYes() {
        PregnancyPage page = new PregnancyPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectYes();
        System.out.println("Selected yes to pregnant in the last 12 months");
        page.clickNext();
    }

    public void selectPregnancyNo() {
        PregnancyPage page = new PregnancyPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectNo();
        System.out.println("Selected no to pregnant in the last 12 months");
        page.clickNext();
    }

    public void selectDiabetesYes() {
        DiabetesPage page = new DiabetesPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectYes();
        System.out.println("Selected yes to diabetes");
        page.clickNext();
    }

    public void selectGlaucomaYes() {
        GlaucomaPage page = new GlaucomaPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectYes();
        System.out.println("Selected yes to glaucoma");
        page.clickNext();
    }

    public void selectGlaucomaNo() {
        GlaucomaPage page = new GlaucomaPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.selectNo();
        System.out.println("Selected no to glaucoma");
        page.clickNext();
    }

    public void assertResultsAreDisplayed(){
        ResultsPage page = new ResultsPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.assertResultsAreVisible();
    }

    public void assertUniversalCreditResultsAreDisplayed(){
        ResultsUniversalCreditPage page = new ResultsUniversalCreditPage(driver);
        Assert.assertEquals(driver.getCurrentUrl(), page.url, "Current url is different from expected");
        page.assertResultVisible();
    }


}
