package base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import pageObjects.costsChecker.StartPage;

import java.util.Arrays;

public class TestBase {

    protected StartPage startPage;
    protected UiInteractions interactions;
    public static WebDriver driver;

    @Parameters({"browserType"})
    @BeforeTest
    public void setupDriver(String browserType) {
        try {
            setDriver(browserType);
        } catch (Exception e) {
            System.out.println("Error setting up driver....." + Arrays.toString(e.getStackTrace()));
        }
    }

    @Parameters({"url"})
    @BeforeClass
    public void loadPageInitWait(String url) {
        driver.get(url);
        interactions = new UiInteractions(driver);
        startPage = new StartPage(driver);
    }

    @AfterTest
    public void tearDown(){
        driver.quit();
    }

    private void setDriver(String browserType) {
        if ("chrome".equals(browserType)) {
            driver = initChromeDriver();

        } else if ("firefox".equals(browserType)) {
            driver = initFirefoxDriver();

        } else {
            System.out.println("browser type << " + browserType + " >> is invalid, Launching Chrome by default..");
            driver = initChromeDriver();
        }
    }

    private WebDriver initChromeDriver() {
        System.out.println("Launching CHROME ...");

        System.setProperty("webdriver.chrome.driver", "src/main/drivers/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        return driver;
    }

    private WebDriver initFirefoxDriver() {
        System.out.println("Launching FIREFOX ...");
        System.setProperty("webdriver.gecko.driver", "src/main/drivers/geckodriver.exe");

        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        return driver;
    }

}
