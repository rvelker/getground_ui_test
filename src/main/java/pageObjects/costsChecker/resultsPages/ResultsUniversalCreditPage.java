package pageObjects.costsChecker.resultsPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import pageObjects.costsChecker.base.CostsCheckerPageBase;

public class ResultsUniversalCreditPage extends CostsCheckerPageBase {

    public ResultsUniversalCreditPage(WebDriver driver) {
        super(driver);
        this.url = baseUrl + "/result-claiming-qualifying-universal-credit";
        System.out.println("Results UC page init");
    }

    @FindBy(css = "#content > div.grid-row > div > div.done-panel")
    public WebElement pnlDone;

    public void assertResultVisible(){
        Assert.assertTrue(waitForElement(pnlDone).isDisplayed(),
                "Results panel should be displayed");
    }
}
