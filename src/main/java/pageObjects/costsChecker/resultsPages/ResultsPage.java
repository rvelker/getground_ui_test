package pageObjects.costsChecker.resultsPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjects.costsChecker.base.CostsCheckerPageBase;

public class ResultsPage extends CostsCheckerPageBase {

    public ResultsPage(WebDriver driver) {
        super(driver);
        this.url = baseUrl + "/result";
        System.out.println("Results page init");
    }

    @FindBy(css = "#result-heading")
    public WebElement headingResults;

    public void assertResultsAreVisible() {}

}
