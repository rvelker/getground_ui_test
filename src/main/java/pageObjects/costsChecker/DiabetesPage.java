package pageObjects.costsChecker;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjects.costsChecker.base.CostsCheckerPageBase;

public class DiabetesPage extends CostsCheckerPageBase {

    public DiabetesPage(WebDriver driver) {
        super(driver);
        this.url = baseUrl +  "/diabetes";
        System.out.println("Diabetes page init");
    }
    @FindBy(id = "label-yes")
    public WebElement btnYes;

    @FindBy(id = "label-no")
    public WebElement btnNo;

    public void selectYes(){
        waitForElement(btnYes).click();
    }

    public void selectNo(){
        waitForElement(btnNo).click();
    }


}
