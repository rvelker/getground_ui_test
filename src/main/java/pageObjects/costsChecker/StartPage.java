package pageObjects.costsChecker;

import org.openqa.selenium.WebDriver;
import pageObjects.costsChecker.base.CostsCheckerPageBase;


public class StartPage extends CostsCheckerPageBase {

    public StartPage(WebDriver driver) {
        super(driver);
        this.url = baseUrl + "/start";
        System.out.println("Start Page init");
    }

    public void beginCheck(){
        clickNext();
    }
}
