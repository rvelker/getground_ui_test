package pageObjects.costsChecker;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjects.costsChecker.base.CostsCheckerPageBase;

public class PregnancyPage extends CostsCheckerPageBase {

    public PregnancyPage(WebDriver driver) {
        super(driver);
        this.url = baseUrl + "/pregnant-or-given-birth";
        System.out.println("Pregnancy page init");
    }
    @FindBy(id = "label-yes")
    public WebElement btnYes;

    @FindBy(id = "label-no")
    public WebElement btnNo;

    public void selectYes(){
        waitForElement(btnYes).click();
    }

    public void selectNo(){
        waitForElement(btnNo).click();
    }
}
