package pageObjects.costsChecker.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CostsCheckerPageBase {

    protected final String baseUrl = "https://services.nhsbsa.nhs.uk/check-for-help-paying-nhs-costs";
    protected WebDriver driver;
    protected WebDriverWait wait;
    public String url;

    public CostsCheckerPageBase(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 5);
    }

    protected WebElement waitForElement(WebElement element){
        return wait.until(ExpectedConditions.visibilityOf(element));
    }

    @FindBy(id = "next-button")
    protected WebElement btnNext;

    public void clickNext(){
        waitForElement(btnNext).click();
    }


}
