package pageObjects.costsChecker.benefitsPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjects.costsChecker.base.CostsCheckerPageBase;

public class UCSpecialCircumstancesPage extends CostsCheckerPageBase {

    public UCSpecialCircumstancesPage(WebDriver driver) {
        super(driver);
        this.url = baseUrl + "/universal-credit-claim";
        System.out.println("UC Special Circumstances Page init");
    }

    @FindBy(id = "label-yes")
    public WebElement btnYes;

    @FindBy(id = "label-no")
    public WebElement btnNo;

    public void selectYes(){
        waitForElement(btnYes).click();
    }

    public void selectNo(){
        waitForElement(btnNo).click();
    }
}
