package pageObjects.costsChecker.benefitsPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjects.costsChecker.base.CostsCheckerPageBase;

public class UniversalCreditPage extends CostsCheckerPageBase {

    public UniversalCreditPage(WebDriver driver) {
        super(driver);
        this.url = baseUrl + "/paid-universal-credit";
        System.out.println("Universal Credit Page init");
    }

    @FindBy(css = "#paidUniversalCredit_fieldset > label:nth-child(3)")
    public WebElement btnYes;

    @FindBy(css = "#paidUniversalCredit_fieldset > label.block-label.selection-button-radio.radio-group.selected")
    public WebElement btnNotYet;

    @FindBy(css = "#paidUniversalCredit_fieldset > label:nth-child(6)")
    public WebElement btnDifferentBenefits;

    public void selectYes(){
        waitForElement(btnYes).click();
    }

    public void selectNotYet(){
        waitForElement(btnNotYet).click();
    }

    public void selectDifferentBenefits(){
        waitForElement(btnDifferentBenefits).click();
    }

}
