package pageObjects.costsChecker.benefitsPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjects.costsChecker.base.CostsCheckerPageBase;

public class TakeHomePayPage extends CostsCheckerPageBase {

    public TakeHomePayPage(WebDriver driver) {
        super(driver);
        this.url = baseUrl + "/universal-credit-take-home-pay";
        System.out.println("Take Home Pay Page init");
    }

    @FindBy(id = "label-yes")
    public WebElement btnYes;

    @FindBy(id = "label-no")
    public WebElement btnNo;

    public void selectYes(){
        waitForElement(btnYes).click();
    }

    public void selectNo(){
        waitForElement(btnNo).click();
    }
}
