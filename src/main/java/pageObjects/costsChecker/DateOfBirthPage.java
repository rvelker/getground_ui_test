package pageObjects.costsChecker;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import pageObjects.costsChecker.base.CostsCheckerPageBase;

public class DateOfBirthPage extends CostsCheckerPageBase {

    public DateOfBirthPage(WebDriver driver) {
        super(driver);
        this.url = baseUrl + "/date-of-birth";
        System.out.println("Date of Birth Page init");
    }

    @FindBy(id = "dob-day")
    public WebElement txtDay;

    @FindBy(id = "dob-month")
    public WebElement txtMonth;

    @FindBy(id = "dob-year")
    public WebElement txtYear;

    public void enterDateOfBirthUnderThirty() {
        String day = "1";
        String month = "1";
        String year = "2000";
        waitForElement(txtDay).sendKeys(day);
        waitForElement(txtMonth).sendKeys(month);
        waitForElement(txtYear).sendKeys(year);
        Assert.assertEquals(txtDay.getAttribute("value"), day, "Incorrect value displayed in day text box");
        Assert.assertEquals(txtMonth.getAttribute("value"), month, "Incorrect value displayed in month text box");
        Assert.assertEquals(txtYear.getAttribute("value"), year, "Incorrect value displayed in year text box");
    }

    public void enterDateOfBirthOverThirty() {
        String day = "1";
        String month = "1";
        String year = "1970";
        waitForElement(txtDay).sendKeys(day);
        waitForElement(txtMonth).sendKeys(month);
        waitForElement(txtYear).sendKeys(year);
        Assert.assertEquals(txtDay.getAttribute("value"), day, "Incorrect value displayed in day text box");
        Assert.assertEquals(txtMonth.getAttribute("value"), month, "Incorrect value displayed in month text box");
        Assert.assertEquals(txtYear.getAttribute("value"), year, "Incorrect value displayed in year text box");
    }
}
