package pageObjects.costsChecker;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjects.costsChecker.base.CostsCheckerPageBase;

public class CountryPage extends CostsCheckerPageBase {

    public CountryPage(WebDriver driver) {
        super(driver);
        this.url = baseUrl + "/where-you-live";
        System.out.println("Country Page init");
    }

    @FindBy(id = "label-wales")
    public WebElement btnWales;

    public void selectWales(){
        waitForElement(btnWales).click();
    }
}
