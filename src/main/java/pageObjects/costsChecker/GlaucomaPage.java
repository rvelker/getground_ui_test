package pageObjects.costsChecker;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageObjects.costsChecker.base.CostsCheckerPageBase;

public class GlaucomaPage extends CostsCheckerPageBase {

    public GlaucomaPage(WebDriver driver) {
        super(driver);
        this.url = baseUrl + "/glaucoma";
        System.out.println("Glaucoma page init");
    }

    @FindBy(id = "label-yes")
    public WebElement btnYes;

    @FindBy(id = "label-no")
    public WebElement btnNo;

    public void selectYes(){
        waitForElement(btnYes).click();
    }

    public void selectNo(){
        waitForElement(btnNo).click();
    }
}
